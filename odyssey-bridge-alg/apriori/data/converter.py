# Authors : Daniel and Sacha

import xlrd
import csv
import sys
import os

def csv_from_excel():
    wb = xlrd.open_workbook(sys.argv[1])
    sheet_names = wb.sheet_names()
    my_csv_file = open('./csvs/'+ os.path.basename(sys.argv[1]).replace(" ", "_") + ".csv", 'w+')
    for sheet in sheet_names:
        sh = wb.sheet_by_name(sheet)
        wr = csv.writer(my_csv_file, quoting=csv.QUOTE_ALL)
        for rownum in range(sh.nrows):
            wr.writerow(sh.row_values(rownum))

    my_csv_file.close()

csv_from_excel()
