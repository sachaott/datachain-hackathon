var AWS = require('aws-sdk');

var s3 = new AWS.S3();


exports.handler =  async (event) => {
    // TODO implement
    var param = {Bucket: 'open-data-odyssey'};
    var s3List = s3.listObjects(param).promise();
    
    let result = await s3List.then(function(data) {
      return data ;
    }).catch(function(err) {
      return err;
    });
   
    const { Contents = [] } = result;
    
  const getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
  }
    
    const removeKey = (item) => {
      const r = getRandomInt(3);
      let link = item.Key;
      let status = '<i style="color:red;" class="fas fa-times-circle"></i>';
      if (r == 1) {
        status = '<i style="color:#7FFF00;" class="fas fa-check-circle"></i>';
        link = '<a target="_BLANK" href="https://s3.eu-west-3.amazonaws.com/open-data-odyssey/'+item.Key+'">'+item.Key+'</a>';
      }
      if (r == 2) status = 'Processing file';
    
      
      
      let  date = new Date(item.LastModified);
      let fdate = date.toLocaleDateString();
      return  [ link, fdate , (item.Size / 1000000).toFixed(2)+ ' MB', status ];
    }

    let resultArray = [];
    Contents.forEach((item) => {
      resultArray.push(removeKey(item))
    })
    
    
    const response = {
        data: resultArray,
    };
    return response;
};
