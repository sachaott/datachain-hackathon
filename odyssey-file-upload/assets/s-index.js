$(document).ready(function() {
    $('#dataset_files').DataTable( {
        "ajax": {
            "type": "GET",
            "url": "https://8nqdj9kmp8.execute-api.eu-west-3.amazonaws.com/Prod/getS3files",
            "crossDomain": true,
            "contentType": "application/json"
        },
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
         
    } );
} );