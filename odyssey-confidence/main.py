import pandas as pd
import json
from pandas.io.json import json_normalize
from mlxtend.preprocessing import TransactionEncoder
from mlxtend.frequent_patterns import apriori


'''

MAIN 

'''    
def main():
    print('--- Start ---')
    file = 'data/Voorbeeld.csv'
    print('--- JSON Loaded  & DF ---')
    df = pd.read_csv(file)
    ds = df.values.tolist()

    dataset = []
    for item in ds:
        dataset.append(str(item))

    print('--- A priori  ---')
    te = TransactionEncoder()
    te_ary = te.fit(dataset).transform(dataset)

    df = pd.DataFrame(te_ary, columns=te.columns_)
    
    frequent_itemsets = apriori(df, min_support=0.6)
    print(frequent_itemsets)
    df.to_csv('temp_result.csv', sep='\t')

if __name__ == '__main__':
    main()